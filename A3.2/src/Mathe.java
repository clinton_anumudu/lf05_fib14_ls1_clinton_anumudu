
public class Mathe {
	
	static double quadrat(double x) {
		double q = x * x;
		return q;
	}
	
	static double hypotenuse(double kathete1, double kathete2) {
		double h = Math.sqrt(quadrat(kathete1) + quadrat(kathete2));
		return h;
	}

}
