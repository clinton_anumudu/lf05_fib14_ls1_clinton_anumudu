import java.util.Scanner;

public class PCHaendler {

	public static void main(String[] args) {
		
		String artikel = liesString("Was möchten Sie bestellen?");
		
		int anzahl = liesInt("Geben Sie die Anzahl ein:");
		
		double preis = liesDouble("Geben Sie den Nettopreis ein:");
		
		double mwst = liesDouble("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		
		double nettogesamtpreis = berechneGesamtNettoPreis(anzahl, preis);
		double bruttogesamtpreis = berechneGesamtBruttoPreis(nettogesamtpreis, mwst);
		
		rechnungausgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);

	}
	
	static String liesString(String text) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);
		String input = myScanner.next();
		return input;
	}
	
	static int liesInt(String text) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);
		int input = myScanner.nextInt();
		return input;
	}
	
	static double liesDouble(String text) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);
		double input = myScanner.nextDouble();
		return input;
	}
	
	static double berechneGesamtNettoPreis(double anzahl, double preis) {
		double netto = anzahl * preis;
		return netto;
	}
	
	static double berechneGesamtBruttoPreis(double netto, double mwst) {
		double brutto = netto * (1 + mwst / 100);
		return brutto;
	}
	
	static void rechnungausgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis, double mwst) {
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
	}

}
