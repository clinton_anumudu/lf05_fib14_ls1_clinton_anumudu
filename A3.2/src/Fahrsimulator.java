import java.util.Scanner;

public class Fahrsimulator {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		double geschwindigkeit = 0.0;
		while (true) {
			System.out.println("Geschwindigkeit: " + geschwindigkeit + "\n");
			System.out.print("Beschleunigen/Bremsen: ");
			double dv = myScanner.nextDouble();
			geschwindigkeit = beschleunige(geschwindigkeit, dv);
			System.out.print("\n\n");
		}
	}
	
	static double beschleunige(double v, double dv) {
		double hoechstgeschwindigkeit = 130.0;
		double kleinstegeschwindigkeit = 0.0;
		double neu = v + dv;
		if (neu < kleinstegeschwindigkeit) {
			neu = kleinstegeschwindigkeit;
		}
		if (neu > hoechstgeschwindigkeit) {
			neu = hoechstgeschwindigkeit;
		}
		return neu;
	}

}
