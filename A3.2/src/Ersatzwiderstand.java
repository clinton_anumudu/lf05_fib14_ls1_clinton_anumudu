
public class Ersatzwiderstand {
	
	static double reihenschaltung(double r1, double r2) {
		double e = r1 + r2;
		return e;
	}
	
	static double parallelschaltung(double r1, double r2) {
		double e = (r1 * r2)/(r1 + r2);
		return e;
	}

}
