
public class Mittelwert {

   public static void main(String[] args) {
	   
	   double x = 2.0;
	   double y = 4.0;
	   double m;
	   
	   m = berechneMittelwert(x, y);
	   
	   System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);
   
   }
   
   static double berechneMittelwert(double x, double y) {
	   double m = (x + y) / 2.0;
	   return m;
   }
   
}
