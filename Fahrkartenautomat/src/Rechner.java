import java.util.Scanner;

public class Rechner {
	
	public static void main(String[] args) {
		
		Scanner myScanner = new Scanner(System.in);
		
		System.out.print("Bitte geben Sie eine ganze Zahl ein: ");
		
		int zahl1 = myScanner.nextInt();
		
		System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");
		
		int zahl2 = myScanner.nextInt();
		
		int ergebnis = zahl1 + zahl2;
		System.out.println("\nErgebnis der Addition lautet: " + ergebnis);
		
		ergebnis = zahl1 - zahl2;
		System.out.println("Ergebnis der Subtraktion lautet: " + ergebnis);
		
		ergebnis = zahl1 * zahl2;
		System.out.println("Ergebnis der Multiplikation lautet: " + ergebnis);
		
		ergebnis = zahl1 / zahl2;
		System.out.println("Ergebnis der Division lautet: " + ergebnis);
		
		myScanner.close();
		
	}
	
}
